import React, { Component } from 'react';
import './App.css';
import {Button, Navbar, Nav, Form, FormControl} from "react-bootstrap";
import Axios from "axios";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Link} from 'react-router-dom';
import View from './components/View';
import Home from './components/Home';
import Add from './components/Add';
import Update from './components/Update';
import Delete from './components/Delete';

export default class App extends Component {

  constructor(){
    super();
    this.state = {
      AllData : [],
      loading : true
    }
  }

  componentDidMount(){
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
    .then(res => {
      this.setState({
        loading : false,
        AllData : res.data.DATA
      })
    })
    .catch(err => {
      console.log(err);
    })
  }

  render() {

    const {loading,AllData} = this.state;

    if(loading){
      return <h1 className="loading">Loading...</h1>
    }

    return (
      <div className="App container-fluid">
        <Router>

          <Navbar bg="dark" variant="dark" className="container">
            <Navbar.Brand as = {Link} to="/">Navbar</Navbar.Brand>
            <Nav className="mr-auto">
            <Nav.Link as = {Link} to="/">Home</Nav.Link>
            </Nav>
            <Form inline>
              <FormControl type="text" placeholder="Search" className="mr-sm-2" />
              <Button variant="outline-info">Search</Button>
            </Form>
          </Navbar>
          <Switch>
            <Route path="/" exact render={()=> <Home data = {AllData}/>}/>
            <Route path="/view/:ID" render={(props)=> <View {...props} data = {AllData}/>}/>
            <Route path="/add/" render={()=> <Add data = {AllData}/>}/>
            <Route path="/update/:ID" render={(props) => <Update {...props} data={AllData}/>}/>
            <Route path="/delete/:ID" render={(props) => <Delete {...props} data={AllData}/>}/>
          </Switch>

        </Router>
      </div>
    )
  }
}

