import React from 'react';
import Axios from "axios";
import {Link} from 'react-router-dom';
import {Button} from "react-bootstrap";

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            titleError: "",
            descError: "",
            checkTitle: false,
            checkDesc: false
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = (e) => {
       e.preventDefault();
        
       const form = {
        title: this.state.title,
        description: this.state.description,
       };

        if(form.title === ""){
            this.setState({
              checkTitle:true,
              titleError:"*Please Input your title"
            })
        }

        if (!form.title.match(/^[a-zA-Z ]*$/)) {
          this.setState({
            checkTitle: true,
            titleError: "*Please enter alphabet characters only."
          })
        }
        if (form.title.match(/^[a-zA-Z ]*$/) && form.title !== ""){
            this.setState({
              checkTitle: false,
              titleError: ""
            })
        }

        if(form.description === ""){
            this.setState({
              checkDesc:true,
              descError:"*Please input your description"
            })
        }

        if (form.description !== ""){
            this.setState({
                checkDesc: false,
                descError:""
            })
        }

        if(form.title.match(/^[a-zA-Z ]*$/) && form.title !== "" && form.description !== ""){

            const postData = {
                TITLE : form.title,
                DESCRIPTION : form.description,
                IMAGE : form.image
            };
    
            Axios.post("http://110.74.194.124:15011/v1/api/articles", postData)
            .then(res => {
                alert(res.data.MESSAGE);
            });

            this.setState({
                title: "",
                description: "",
                checkTitle : false,
                checkDesc : false,
                titleError : "",
                descError : ""
           });
        }
    }

    render() {
        return (
            <div className="container add-data">
                <section className="button-back">
                    <Link to="/">
                        <Button>Back</Button>
                    </Link>
                </section>
                <form>
                    <label>Title:</label>
                    <input
                        className="input" 
                        name='title'
                        value={this.state.title}
                        onChange={e => this.handleChange(e)}
                    />
                    <div 
                        className={this.state.checkTitle ? 
                        "show":"none"} 
                        style={{marginLeft:"13px",fontSize:"18px",color:"#C62828"}}>
                        &#x00021;{this.state.titleError}
                    </div>
                    <label>Description:</label>
                    <input
                        className="input" 
                        name='description'
                        value={this.state.description} 
                        onChange={e => this.handleChange(e)}
                    />
                    <div 
                        className={this.state.checkDesc ? 
                        "show":"none"} 
                        style={{marginLeft:"13px",fontSize:"18px",color:"#C62828",marginBottom:"24px"}}>
                        &#x00021;{this.state.descError}
                    </div>
                    <button onClick={(e) => this.onSubmit(e)} className="add">Send</button>         
                </form>
            </div>
        );
    }
}

export default Add;