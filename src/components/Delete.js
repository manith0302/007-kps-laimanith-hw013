import React from 'react';
import Axios from "axios";
import {Link} from 'react-router-dom';
import {Button, Row, Col} from "react-bootstrap";

function Delete({match, data}) {

    let dataView = data.find((d)=> d.ID == match.params.ID);

    const id = match.params.ID;


    Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
        .then(res => {
        alert(res.data.MESSAGE)
    })

    return (
        <div>
            <section className="container button-back">
                <Link to="/">
                    <Button>Back</Button>
                </Link>
            </section>
            <section className="container App Card">
                <Row>
                    <Col>
                        {dataView.TITLE}
                    </Col>
                    <Col>
                        <img src={dataView.IMAGE} onError={(e) => {e.target.src = "https://www.ajactraining.org/wp-content/uploads/2019/09/image-placeholder.jpg"}} />
                    </Col>
                    <Col>
                        {dataView.DESCRIPTION}
                    </Col>
                </Row>
            </section>
        </div>
    );
}
export default Delete;
