import React from 'react';
import {Link} from 'react-router-dom';
import {Button, Table} from "react-bootstrap";

function Home({data}) {

    const AllData = data.map((d) => 
        <tr key={d.ID}>
            <td>{d.ID}</td>
            <td>{d.TITLE}</td>
            <td>{d.DESCRIPTION}</td>
            <td>{d.CREATED_DATE}</td>
            <td>
                <img src={d.IMAGE} onError={(e) => {e.target.src = "https://www.ajactraining.org/wp-content/uploads/2019/09/image-placeholder.jpg"}} />
            </td>
            <td className="buttons">
                <Link to={`/view/${d.ID}`}>
                    <Button variant="primary">View</Button>
                </Link>
                <Link to={`/update/${d.ID}`}>
                    <Button variant="warning">Update</Button>
                </Link>
                <Link to={`/delete/${d.ID}`}>
                    <Button variant="danger">Delete</Button>
                </Link>
            </td>
        </tr>
    )

    return (
        <div>
            <h1>Article Management</h1>
            <Link to="/add">
                <Button variant="outline-light">Add New Article</Button>
            </Link>

            <Table striped bordered hover variant="dark" className="container table-api">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Created Date</th>
                    <th>Image</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {AllData}
                </tbody>
            </Table>
        </div>
    )
}

export default Home
