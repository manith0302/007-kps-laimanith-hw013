import React from 'react';
import Axios from "axios";
import {Link} from 'react-router-dom';
import {Button} from "react-bootstrap";

class Update extends React.Component {
    constructor({match, data}) {
        super();
        let dataUpdate = data.find((d)=> d.ID == match.params.ID)
        this.state = {
            title: dataUpdate.TITLE,
            description: dataUpdate.DESCRIPTION,
            image : dataUpdate.IMAGE
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

   onSubmit = (e) => {
       e.preventDefault();

       const match = this.props.match;

       const form = {
        title: this.state.title,
        description: this.state.description,
       };

        const updateData = {
            TITLE : form.title,
            DESCRIPTION : form.description
        };

        Axios.put(`http://110.74.194.124:15011/v1/api/articles/${match.params.ID}`, updateData)
        .then(res => {
            alert(res.data.MESSAGE);
        });

       this.setState({
            title: '',
            description: ''
       });
    }

    render() {

        const {image} = this.state;
        return (
            <div className="container add-data">

                <section className="button-back">
                    <Link to="/">
                        <Button>Back</Button>
                    </Link>
                </section>
                <section className="updateForm">
                    <section className="form">
                        <form>
                            <label>Title:</label>
                            <input
                                className="input" 
                                name='title'
                                value={this.state.title}
                                onChange={e => this.handleChange(e)}
                            />
                            <label>Description:</label>
                            <input
                                className="input" 
                                name='description'
                                value={this.state.description} 
                                onChange={e => this.handleChange(e)}
                            />
                            <button onClick={(e) => this.onSubmit(e)} className="add">Update</button>         
                        </form>
                    </section>
                    <section>
                        <img src={image} onError={(e) => {e.target.src = "https://www.ajactraining.org/wp-content/uploads/2019/09/image-placeholder.jpg"}} />
                    </section>
                </section>
            </div>
        );
    }
}

export default Update;